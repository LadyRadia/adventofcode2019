mod utils;
mod day1;
mod day2;

fn main() {
    println!("Hello, world!");
    println!("DAY 1: {}", day1::run());
    println!("DAY 2A: {}", day2::run_part_a());
    println!("DAY 2B: {}", day2::run_part_b());
}
