use std::str::FromStr;
use std::io::BufReader;
use super::utils;
use crate::utils::load_type_from_file;

pub struct Program
{
    data: Vec<i32>
}

pub enum Opcode {
    Add,
    Multiply,
    End,
    Data(i32)
}

impl From<i32> for Opcode
{
    fn from(val: i32) -> Self {
        match val {
            1 => Opcode::Add,
            2 => Opcode::Multiply,
            99 => Opcode::End,
            val => Opcode::Data(val),
        }
    }
}

impl FromStr for Program  {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let individual_vals = s.split(",");
        let mut data = vec!();
        for raw_val in individual_vals {
            let val = i32::from_str(raw_val).expect("Invalid data provided in day 2");
            data.push(val);
        }
        Ok(Program{
            data
        })
    }
}

impl Program {
    pub fn set_noun_verb(&mut self, noun: i32, verb: i32) -> Result<(), ()> {
        if self.data.len() < 3 {
            return Err(());
        }
        self.data[1] = noun;
        self.data[2] = verb;
        Ok(())
    }

    pub fn run_intopcode_program(&mut self) -> Result<i32, ()> {
        let mut pc = 0;
        let mut program = self.data.clone();
        loop {
            let opcode = Opcode::from(program[pc]);
            let output_pos = program[pc + 3] as usize;
            let left_input = program[pc + 1] as usize;
            let right_input = program[pc + 2] as usize;
            assert!(output_pos < program.len());
            assert!(left_input < program.len());
            assert!(right_input < program.len());
            match opcode {
                Opcode::Add => program[output_pos] = program[left_input] + program[right_input],
                Opcode::Multiply => program[output_pos] = program[left_input] * program[right_input],
                Opcode::End => {
                    return Ok(program[0])
                },
                Opcode::Data(i32) => return Err(())
            }
            pc += 4;
        }
    }
}

pub fn run_part_a() -> i32
{
    let mut programs = load_type_from_file::<Program>("inputs/day02a.txt");
    assert_eq!(programs.len(), 1);
    let mut program = programs.remove(0);
    program.set_noun_verb(12, 2).expect("Unable to set the pre-1202 program alarm state.");
    program.run_intopcode_program().expect("Error when parsing program")
}

pub fn run_part_b() -> i32
{
    let mut programs = load_type_from_file::<Program>("inputs/day02a.txt");
    assert_eq!(programs.len(), 1);
    let mut program = programs.remove(0);
    let target = 19690720;
    //now we need to algorithmically figure out which nouns and verbs get us to the target
    //we know both the noun and the verb increase it (only possible actions are ADD and MULT)
    //so we have to figure out if it's linear or exponential
    let part_a_result = run_program(&mut program, 12, 2);
    let diff_1_increase = run_program(&mut program, 13, 2) - part_a_result;
    let diff_2_increase = run_program(&mut program, 14, 2) - part_a_result;
    let increases_linearly = diff_1_increase == (diff_2_increase / 2);
    //if it's linear, great, we can just figure this one out with multiplication
    if increases_linearly {
        let noun = {
            let distance = target - part_a_result;
            let steps_needed_for_noun = distance / diff_1_increase;
            steps_needed_for_noun + 12
        };
        let intermediate_run = run_program(&mut program, noun, 0);
        //verb is just added on to the result, so we can just grab the diff and use it.
        let verb = target - intermediate_run;
        let success = run_program(&mut program, noun, verb) == target;
        assert!(success);
        return (noun * 100) + verb;
    } else {
        0
   }
}

pub fn run_program(program: &mut Program, noun: i32, verb: i32) -> i32 {
    program.set_noun_verb(noun, verb).unwrap();
    program.run_intopcode_program().unwrap()
}