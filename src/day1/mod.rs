use std::fs::File;
use std::io::{prelude, Read, BufReader, BufRead};
use std::path::Path;
use super::utils;
use crate::utils::load_type_from_file;

pub struct Module
{
    mass: u32,
}

impl Module
{
    pub fn get_fuel_required(&self) -> u32
    {
        let mut fuel_metric = self.mass;
        let mut acc = 0;
        while fuel_metric > 8 {
            let new_fuel_required = (fuel_metric / 3) - 2;
            acc += new_fuel_required;
            fuel_metric = new_fuel_required;
        }

        return acc;
    }
}

pub fn run() -> u64
{
    let modules = load_type_from_file::<u32>("inputs/day01a.txt")
        .into_iter().map(|module_mass| Module {
        mass: module_mass
    });
    modules.into_iter().map(|module| module.get_fuel_required())
        .fold(u64::min_value(), |acc, fuel_required| acc + fuel_required as u64)
}

#[cfg(test)]
mod tests
{
    use super::*;
    #[test]
    pub fn test_given_example_a() {
        let module = Module {
            mass : 1969
        };
        assert_eq!(966, module.get_fuel_required());
    }
    #[test]
    pub fn test_given_example_b() {
        let module = Module {
            mass : 100756
        };
        assert_eq!(50346, module.get_fuel_required());
    }
}