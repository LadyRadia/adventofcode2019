use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::str::FromStr;
use std::fmt::Debug;

pub fn load_type_from_file<T>(filename: &str) -> Vec<T>
where T: FromStr,
      <T as std::str::FromStr>::Err: std::fmt::Debug
{
    let mut vec = vec!();
    let file_path = Path::new(filename);
    let mut input_file = match File::open(file_path) {
        Err(err) => panic!("couldn't open {}: {}", file_path.display(), err),
        Ok(file) => file
    };
    let reader = BufReader::new(input_file);
    for line in reader.lines() {
        match line {
            Err(err) => panic!("couldn't open {}: {}", file_path.display(), err),
            Ok(line_val) => {
                vec.push(line_val.parse::<T>()
                    .expect(format!("Invalid input in {}: {}", filename, line_val).as_str()));
            }
        }
    }
    vec
}

